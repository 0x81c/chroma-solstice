require "entity"

Ass = class("Ass", Entity)

function Ass:initialize(x, y, color, folder, name)
	local spr = love.graphics.newImage("art/" .. folder .. name .. "_" .. string.lower(string.sub(color, 1, 1)) ..".png")
	Entity.initialize(self, {
		x = x,
		y = y,
		color = color,
		name = name,
		cellShape = spriteCells["tree"][color],
		collision = "unmoveable", --player can't push other player
		sprite = spr
	})
end
