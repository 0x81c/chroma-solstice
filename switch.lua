require "entity"

Switch = class("Switch", Entity)

Switch.static.sprites = {
	red = love.graphics.newImage("art/switch_r.png"),
	green = love.graphics.newImage("art/switch_g.png"),
	blue = love.graphics.newImage("art/switch_b.png"),
	pressed = love.graphics.newImage("art/switch_pressed.png")
}

function Switch:initialize(x, y, color)
	Entity.initialize(self, {
		x = x,
		y = y,
		color = color,
		collision = "none", --switches are floor elements that can't be pushed or bloc movement!!
		sprite = sprites.switch[color]
	})

	self.activated = false
end

function Switch:activate()
	if self.activated == false then 
		self.activated = true
	end
end

function Switch:deactivate()
	if self.activated == true then 
		self.activated = false
	end
end