require "entity"

Door = class("Door", Entity)

function Door:initialize(x, y)
	Entity.initialize(self, {
		x = x,
		y = y,
		color = "white",
		collision = "unmoveable",
		sprite = sprites.door["green"] --doesn't matter for now which we put here b/c, we override the draw function neway
	})
	self.locked = true

	self.colors = {}
	for _, color in pairs({"red", "green", "blue"}) do
		self.colors[color] = true --these are currently loc_ed
	end
end

function Door:draw()
	if self.locked then
		for color, active in pairs(self.colors) do
			if active then
				love.graphics.setColor(gColor[color]:set())
				local p = self:getDrawPos()
				love.graphics.draw(sprites.door[color], p.x + self.drawOffset.x, p.y + self.drawOffset.y, 0, drawScale, drawScale)
			end
		end
		love.graphics.setColor(gColor.white:set())
		local p = self:getDrawPos()
		love.graphics.draw(sprites.door["white"], p.x + self.drawOffset.x, p.y + self.drawOffset.y, 0, drawScale, drawScale)		
	else
		for color, active in pairs(self.colors) do
			love.graphics.setColor(gColor[color]:set())
			local p = self:getDrawPos()
			love.graphics.draw(sprites.door["unlocked"], p.x + self.drawOffset.x, p.y + self.drawOffset.y, 0, drawScale, drawScale)
		end		
	end
end

function Door:setColors(colors)
	local offColors = {}

	for color, off in pairs(colors) do
		if off then
			if self.colors[color] then self.colors[color] = false end
			table.insert(offColors, color)
		else
			if not self.colors[color] then self.colors[color] = true end
		end
	end

	if #offColors == 3 then
		if self.locked then self:unlock() end
	else
		if not self.locked then self:lock() end
	end
end

function Door:unlock()
	self.locked = false
	self.collision = "none"
end

function Door:lock()
	self.locked = true
	self.collision = "unmoveable"
end