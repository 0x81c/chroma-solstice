function constrain(val, low, high)
	if val > high then 
		return high
	elseif val < low then 
		return low
	else 
		return val
	end
end

function map(value, inMin, inMax, outMin, outMax)
	local inPercent = value / (inMax - inMin)
	return (outMax - outMin) * (inPercent) + outMin
end

function lerp(a, b, amount)
	return (1 - amount) * a + (amount * b)
end
