love.graphics.setDefaultFilter("linear", "nearest")

class = require "libs/middleclass"
Timer = require "libs/hump/timer"
sfxr = require "libs/sfxrlua/sfxr"
json = require "libs/json4lua/json/json"
require "_debug_helpers"
require "_helpers"
require "input"
require "color"
require "room"
require "shaders"
require "sprite_manager"

function love.load()
	_initDefaults()
	_initGridVars()
	_initSounds()
	_initGlobalColors()
	processSprites()

	gameCanvas = love.graphics.newCanvas()
	finalCanvas = love.graphics.newCanvas()

	inputTimer = Timer.new()

	currentRoom = Room:new{
		player = {
			x = 5, y = 5, activeColors = {"red", "green", "blue"}
		}
	}
end

function love.update(dt)
	Input:handler(dt)
	currentRoom:update(dt)

	if love.keyboard.isDown("w") then worldSha = worldSha + 1 end
	if love.keyboard.isDown("s") then worldSha = worldSha - 1 end
end

function love.draw()
	gameCanvas:clear()
	finalCanvas:clear()

	love.graphics.setCanvas(gameCanvas)
	love.graphics.setBlendMode("screen")
	currentRoom:draw()

	love.graphics.setCanvas(finalCanvas)

	local scaleCoefficient = .93
	local samples = 7
  	
  	love.graphics.push()

	for i = 1, samples do
		local c = 65 / (i * 1.75)
		love.graphics.setColor(c, c, c)
		love.graphics.scale(scaleCoefficient, scaleCoefficient)
		love.graphics.translate((width / 2 - (width * scaleCoefficient) / 2) + 2, (width / 2 - (height * scaleCoefficient) / 2) + 2)
		love.graphics.draw(gameCanvas, 0, 0)
	end

	love.graphics.pop()
	love.graphics.setColor(255, 255, 255)
	love.graphics.draw(gameCanvas, 0, 0)
	love.graphics.setCanvas()

	love.graphics.setBlendMode("alpha")
	love.graphics.setColor(255, 255, 255, 255)
	love.graphics.draw(finalCanvas, 0, 0)
	love.graphics.print("Current FPS: "..tostring(love.timer.getFPS( )), 10, 10)

end

function love.keypressed(key)
	if Input.isPlayerMove(key) then
		currentRoom:movePlayer(key)
	elseif Input.isPlayerSwitch(key) then
		local c = Input.playerColor(key)
		currentRoom:switchPlayerColor(c)
	end
	if key == "w" then
		worldSha = worldSha + 1
	end
	if key == "s" then
		worldSha = worldSha - 1
	end
end

function _initDefaults()
	--set filter
	love.graphics.setDefaultFilter("linear", "nearest")
	love.filesystem.createDirectory("rooms")
	
	--set global width / height variables
	width = love.graphics.getWidth()
	height = love.graphics.getHeight()
	if width > height then
		width = height
	else
		height = width
	end

	--set randomseed
	math.randomseed(os.time())
end

function _initGridVars()
	gridWidth, gridHeight = 11, 11
	drawScale = width / gridWidth / 16

	worldSha = 1
end

function _initGlobalColors()
	gColor = {}
	gColor.red, gColor.green, gColor.blue, gColor.white = Color:new("red"), Color:new("green"), Color:new("blue"), Color:new("white")
end

function _initSounds()
	staticWave = sfxr.newSound()
	staticWave.wavetype = 3
	staticWave.frequency.start = .1
	staticWave.envelope.release = 0
	staticWave.envelope.sustain = 1
	staticWave.envelope.decay = 0
	static = love.audio.newSource(staticWave:generateSoundData())
	static:setLooping(true)
	static:setVolume(.15)
	static:play()
end

