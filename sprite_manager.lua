sprites = {
	player = love.graphics.newImage("art/player.png"),
	wall = love.graphics.newImage("art/static_wall.png"),
	box = {
		red = love.graphics.newImage("art/wall_r.png"),
		green = love.graphics.newImage("art/wall_g.png"),
		blue = love.graphics.newImage("art/wall_b.png")
	},
	door = {
		white = love.graphics.newImage("art/door_locked_w.png"),
		red = love.graphics.newImage("art/door_locked_r.png"),
		green = love.graphics.newImage("art/door_locked_g.png"),
		blue = love.graphics.newImage("art/door_locked_b.png"),
		unlocked = love.graphics.newImage("art/door_unlocked.png")
	},
	switch = {
		red = love.graphics.newImage("art/switch_r.png"),
		green = love.graphics.newImage("art/switch_g.png"),
		blue = love.graphics.newImage("art/switch_b.png")
	},
	water = {
		red = love.graphics.newImage("art/nature/water_r.png"),
		green = love.graphics.newImage("art/nature/water_g.png"),
		blue = love.graphics.newImage("art/nature/water_b.png")
	},
	mac_smaller = love.graphics.newImage("art/tech/processed/mac_smaller_r.png"),
	tree = {
		red = love.graphics.newImage("art/nature/palm_scaled_r.png"),
		green = love.graphics.newImage("art/nature/palm_scaled_g.png"),
		blue = love.graphics.newImage("art/nature/palm_scaled_b.png")
	},
	test_object = love.graphics.newImage("art/test_object.png"),
	npcs = {

	},
	artifact = love.graphics.newImage("art/process/cone.png"),
}

spriteCells = {}


function processSprites()
	getCellsFromSprite(sprites.player)

	for name, spriteObj in pairs(sprites) do
		print(name)
		if type(spriteObj) == "table" then
			spriteCells[name] = {}
			--this is multicolored so...
			for color, sprite in pairs(spriteObj) do
				spriteCells[name][color] = getCellsFromSprite(sprite)
			end
		else
			--assume it's an image obj
			spriteCells[name] = getCellsFromSprite(spriteObj)
		end
	end
end

function getCellsFromSprite(sprite)
	local w, h = sprite:getWidth(), sprite:getHeight()
	local box = {w = w / 16, h = h / 16}
	local boxCells = cellShapeFromBox(box)
	if box.w < 1 or box.h < 1 then
		return boxCells
	end
	
	local img = sprite:getData()

	local cellsToRemove = {}
	local stop

	for _, cell in pairs(boxCells) do

		local startPixel = {x = (cell.x - 1) * 16, y = (cell.y - 1) * 16}
		local endPixel = {x = (cell.x * 16) - 1, y = (cell.y * 16) - 1}
		
		local empty = true

		for i = startPixel.x, endPixel.x do
			for j = startPixel.y, endPixel.y do
				local r, g, b, a = img:getPixel(i, j)
				if a > 0 and (r > 0 or g > 0 or b > 0) then
					--this cell has color so
					empty = false
				end
			end
		end

		if empty then 
			print("empty @ ", cell.x, cell.y)
			cellsToRemove[cell] = cell --references boxCells cell so comparison should wor_
		end
	end

	local i = 1
	while i <= #boxCells do
		if boxCells[i] == cellsToRemove[boxCells[i]] then
			table.remove(boxCells, i)
		else
			i = i + 1
		end
	end

	shallowTablePrint(boxCells)
	return boxCells
end

function cellShapeFromBox(box)
	local cells = {}
	for i = 1, box.w do
		for j = 1, box.h do
			table.insert(cells, {x = i, y = j})
		end
	end
	return cells
end
