function shallowTablePrint(t)
	for i, v in pairs(t) do
		print(i, v)
	end
end

function printMatrix(matrix)
	for i, v in ipairs(matrix) do
		for j, w in ipairs(v) do
			if not w then io.write("o ") else io.write("x ") end
		end
		io.write("\n")
	end
end

function randomPosition()
	return math.random(1, gridWidth), math.random(1, gridHeight)
end