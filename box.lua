require "entity"

Box = class("Box", Entity)

function Box:initialize(x, y, color)
	Entity.initialize(self, {
		x = x,
		y = y,
		sizeX = 1,
		sizeY = 1,
		color = color,
		collision = "moveable",
		sprite = sprites.box[color]
	})
end

function Box:update()

end

function Box:tick()

end