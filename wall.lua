require "entity"

Wall = class("Wall", Entity)

function Wall:initialize(x, y, color)
	Entity.initialize(self, {
		x = x,
		y = y,
		sizeX = 1,
		sizeY = 1,
		color = color,
		cellShape = spriteCells.wall,
		collision = "unmoveable",
		sprite = sprites.wall
	})
end

function Wall:update(dt)

end

function Wall:tick()

end