Input = {}

Input.playermovekeys = { up = "up", down = "down", left = "left", right = "right" }
Input.playerswitchkeys = { ["1"] = "1", ["2"] = "2", ["3"] = "3"}
Input.colorfromkey = {"red", "green", "blue"}

function Input:handler(dt)

end

function Input.isPlayerMove(key)
	if Input.playermovekeys[key] then
		return true
	end
	return false
end

function Input.isPlayerSwitch(key)
	if Input.playerswitchkeys[key] then
		return true
	end
	return false
end

function Input.playerColor(key)
	return Input.colorfromkey[tonumber(key)]
end