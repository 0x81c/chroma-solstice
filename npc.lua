npc = class("npc", Entity)

function npc:initialize(x, y, color, name)
	Entity.initialize(self, {
		x = x,
		y = y,
		color = color,
		collision = "unmoveable",
		sprite = sprites.npc[name][color],
		onBump = function(self)
			
		end,
	})
end