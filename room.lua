Room = class("Room")

require "libs/TSerial"
require "player"
require "box"
require "wall"
require "artifact"
require "npc"
require "art"
require "assorted"
require "switch"
require "door"
require "dialogue_manager"

function Room:initialize(t)
	self.player = {}
	self.switches = { red = {}, green = {}, blue = {} }
	self.doors = {}
	self.collideables = { red = {}, green = {}, blue = {} }
	self.activeColors = {red = false, green = false, blue = false}
	self.colorsPlayerHas = {}

	if t.name then
		print("loaded!!")
		self:load(t.name)
		for _, color in ipairs(t.player.activeColors) do
			self:registerPlayer(Player:new(t.player.x, t.player.y, color), color)
			self:setActiveColor(color, true)
		end
		self.exits = t.exits
		self.name = t.name
	else
		self.colorsPlayerHas = {"red", "green", "blue"}
		

		for _, color in ipairs(t.player.activeColors) do
			self.player[color] = Player:new(t.player.x, t.player.y, color)
		end

		self.activeColors = {}
		for _, color in ipairs(t.player.activeColors) do self.activeColors[color] = true end

		-- for color, coords in pairs(t.player) do
		-- 	self.player[color] = Player:new(coords.x, coords.y, color)
		-- 	self.activeColors[color] = true
		-- end

		for i=1, 1 do
			local x, y = randomPosition()
			-- local door = Door:new(x, y, self.colorsPlayerHas)
			-- table.insert(self.doors, door)

			for color, active in pairs(self.activeColors) do
				table.insert(self.collideables[color], Wall:new(x, y, color))
			end
		end

		for i=0, 5 do
			local x, y = randomPosition()
			for color, active in pairs(self.activeColors) do
				table.insert(self.collideables[color], Box:new(x, y, color))
			end
		end

		-- for i = 0, 5 do
		-- 	local x, y = randomPosition()
		-- 	print(x, y)
		-- 	local c = randomColor()
		-- 	table.insert(self.switches[c], Switch:new(x, y, c))
		-- end

		for color, active in pairs(self.activeColors) do
			local x, y = 6, 3
			-- if active then self.collideables[color]["n: " .. x .. ", " .. y] = Ass:new(x, y, color, "animals/processed/", "me_rach") end

			local x, y = 1, 1
			if active then table.insert(self.collideables[color], Ass:new(x, y, color, "nature/", "palm_scaled_reduced")) end

			-- local x, y = 7, 7
			-- if active then self.collideables[color]["n: " .. x .. ", " .. y] = Ass:new(x, y, color, "food/processed/", "beer_mug") end

			-- self.collideables[color]["fpp"] = Artifact:new(6, 6, color)
		end
	end

	self.switchMatrices = {}
	self.collidableMatrices = {} --{red = {{...}{...}{...}}, ...
	self:createSwitchMatrix()
	self:createCollideablesMatrix()
end


function Room:update(dt)
	for color, player in pairs(self.player) do
		player:update(dt)
	end
end

function Room:draw()

	for color, player in pairs(self.player) do
		player:draw()
	end
	for color, entityArray in pairs(self.collideables) do
		for _, entity in pairs(entityArray) do
			entity:draw()
		end
	end
	for color, switchArray in pairs(self.switches) do
		for _, switch in pairs(switchArray) do
			switch:draw()
		end
	end
end

function Room:createCollideablesMatrix()
	for color, entities in pairs(self.collideables) do
		self.collidableMatrices[color] = self:createEntityMatrix(entities)
	end
end

function Room:createSwitchMatrix()
	for color, switches in pairs(self.switches) do
		self.switchMatrices[color] = self:createEntityMatrix(switches)
	end
end

function Room:createEntityMatrix(entities)
	local matrix = {}

	--lets populate it w/ empty stuff first
	for i = 1, gridWidth do
		local row = {}
		for i = 1, gridHeight do
			table.insert(row, false)
		end
		table.insert(matrix, row)
	end


	--assumes that entities table is set up li_e {index = ent1, anotherIndex = ent2, ...}
	for _, entity in pairs(entities) do
		for _, occupiedCells in pairs(entity:getOccupiedCells()) do
			--we store a table reference so we can call a method on it during a chec_
			matrix[occupiedCells.y][occupiedCells.x] = entity
		end	
	end
	-- printMatrix(matrix)

	return matrix
end

function Room:getCellInMatrix(matrix, cell)
	return matrix[cell.y][cell.x]
end

function Room:movePlayer(key)
	
	local moved = false
	local previousPositions = {} --these are so we can perform the overlap chec_
	local newPositions = {} 

	for _, color in pairs(self:getActiveColors()) do
		local entitiesToPush, entitiesToPull = {}, {}
		local pos = self.player[color]:getGridPos()
		previousPositions[color] = pos
		local x, y = pos.x, pos.y
		local dir = {x = 0, y = 0}
		local axis

		if key == "up" then
			dir.y, axis = -1, "y"
		elseif key == "down" then
			dir.y, axis = 1, "y"
		elseif key == "left" then
			dir.x, axis = -1, "x"
		elseif key == "right" then
			dir.x, axis = 1, "x"
		end

		local move = true
		-- PUSHHHHHHHHHHHHHHHHHHHHHH---------------------

		local initialTargetCell = {
			x = pos.x + dir.x, y = pos.y + dir.y
		} -- this is for reference for each color as we change the targetCells table

		local initialTargetEntity

		if self:isInBounds(initialTargetCell) then
			initialTargetEntity = self:getCellInMatrix(self.collidableMatrices[color], initialTargetCell)
		end

		if initialTargetEntity then initialTargetEntity:bump() end

		if not self:isInBounds(initialTargetCell) then
			-- let's us move out of bounds to Change Rooms without pulling a bloc_
			move = true
		elseif initialTargetEntity and initialTargetEntity:canPassOver() then
			--we should put this in the else statement actually
			move = true
		else
			local targetCells

			targetCells = {initialTargetCell} -- ...we start with our attempted movement...
			local doneSearching
			while not doneSearching do
				nextTargetCells = {} -- ...then we create this array to save our next group of cells
				local emptyCells = {}
				for _, cell in pairs(targetCells) do
					if self:isInBounds(cell) then
						local entity = self:getCellInMatrix(self.collidableMatrices[color], cell)
						if entity then
							--something is here!!
							if not entitiesToPush[entity] then --if the entity hasnt been found already
								if entity:canMove() then
									local projectedCells = entity:getOccupiedCells(dir) -- we project the movement here
									entitiesToPush[entity] = entity
									for _, projectedCell in ipairs(projectedCells) do
										table.insert(nextTargetCells, projectedCell)
									end
								else
									move = false
									doneSearching = true
								end
							end
						else 
							--there's nothin there!!
							table.insert(emptyCells, cell)
						end
					else
						--we are out of bounds now
						move = false
						doneSearching = true
					end
				end

				--if every projected cell is empty then we can do the move
				if #emptyCells == #targetCells then
					move = true
					doneSearching = true
				end

				targetCells = nextTargetCells
			end
			--let's get out of this loop b/c if we can't move one color we can't move at all
		end


		---PULL CHEC

		local initialTargetCell = {x = pos.x - dir.x, y = pos.y - dir.y}

		local targetCells 

		local pull
		--for each color...
		targetCells = {initialTargetCell} -- ...we start with our attempted movement...
		local doneSearching
		while not doneSearching do
			nextTargetCells = {} -- ...then we create this array to save our next group of cells
			local emptyCells = {}
			for _, cell in pairs(targetCells) do
				if self:isInBounds(cell) then
					local entity = self:getCellInMatrix(self.collidableMatrices[color], cell)
					if entity then
						--something is here!!
						if not entitiesToPull[entity] then --if the entity hasnt been found already
							if entity:canMove() then
								local projectedCells = entity:getOccupiedCells(dir) -- we project the movement here
								entitiesToPull[entity] = entity
								for _, projectedCell in ipairs(projectedCells) do
									table.insert(nextTargetCells, projectedCell)
								end
							else
								pull = false
								doneSearching = true
							end
						end
					else 
						--there's nothin there!!
						table.insert(emptyCells, cell)
					end
				else
					--we are out of bounds now
					doneSearching = true
				end
			end

			--if every projected cell is empty then we can do the move
			if #emptyCells == #targetCells then
				pull = true
				doneSearching = true
			end

			targetCells = nextTargetCells
		end

		if move then
			for _, entity in pairs(entitiesToPush) do
				entity:move(axis, dir[axis])
			end
			if pull == true then
				for _, entity in pairs(entitiesToPull) do
					entity:move(axis, dir[axis])
				end
			end
			self.player[color]:move(axis, dir[axis])
			moved = true
		end
		newPositions[color] = self.player[color]:getGridPos()
	end

	if moved then
		self:playerJoinCheck(previousPositions, newPositions)
		self:tick()
	end
end

function Room:switchPlayerColor(color)
	local canSwitch

	if self:onlyColorIsActive(color) then
		canSwitch = false
	else
		for _, c in pairs(self:getPossibleColors()) do
			if c == color then
				self:setActiveColor(c, true)
			else
				self:setActiveColor(c, false)
			end
		end
	end

	self.player[color]:playSwitchSound(canSwitch)
end

function Room:setActiveColor(color, bool)
	self.activeColors[color] = bool
end

function Room:getActiveColors()
	local colors = {}

	for color, active in pairs(self.activeColors) do
		if active then
			table.insert(colors, color)
		end
	end

	return colors
end

function Room:colorIsActive(color)
	if self.activeColors[color] then
		return true
	end
	return false
end

function Room:getInactiveColors()
	local colors = {}

	for color, active in pairs(self.activeColors) do
		if not active then
			table.insert(colors, color)
		end
	end

	return colors
end

function Room:getPossibleColors()
	local colors = {}

	for _, color in ipairs(self.colorsPlayerHas) do
		table.insert(colors, color)
	end

	return colors
end

function Room:onlyColorIsActive(color)
	local activeColors = self:getActiveColors()
	if #activeColors == 1 and activeColors[1] == color then
		return true
	else
		return false
	end
end

function Room:tick()
	for color, entityArray in pairs(self.collideables) do
		for _, entity in pairs(entityArray) do
			entity:tick()
		end
	end
	self:createCollideablesMatrix()
	self:createSwitchMatrix()
	local setDoorColors = self:switchCheck()
	for _, door in pairs(self.doors) do
		door:setColors(setDoorColors)
	end
	self:nextRoomCheck()
end

function Room:playerJoinCheck(previousPositions, newPositions)
	local activate = {}
	local deactivate = {}
	local activeColors = self:getActiveColors()

	for _, color in ipairs(activeColors) do
		if #activeColors > 1 then
			if Entity.positionsAreEqual(previousPositions[color], newPositions[color]) then
				table.insert(deactivate, color)
			end
		end
	end

	for _, inactiveColor in ipairs(self:getInactiveColors()) do
		local overlapping = 0
		local activeColors = self:getActiveColors()
		local inactivePos = self:getPlayerPos(inactiveColor)
		for _, activeColor in ipairs(activeColors) do
			local activePos = self:getPlayerPos(activeColor)
			if activePos.x == inactivePos.x and activePos.y == inactivePos.y then
				overlapping = overlapping + 1
			end
		end
		if overlapping == #activeColors then
			table.insert(activate, inactiveColor)
		end
	end

	for _, color in ipairs(activate) do
		self:setActiveColor(color, true)
	end

	for _, color in ipairs(deactivate) do
		self:setActiveColor(color, false)
	end
end

function Room:getPlayerPos(color)
	return self.player[color]:getGridPos()
end

function Room:getPlayerObject(color)
	return self.player[color]
end

function Room:getDoors()
	return self.doors
end

function Room:getCollideables()
	return self.collideables
end

function Room:getSwitches()
	return self.switches
end

function Room:switchCheck()
	local doorsToUnlock = {}
	for color, switchArray in pairs(self.switches) do
		local onSwitches = {}
		for _, switch in pairs(switchArray) do
			local targetCell = self:getCellInMatrix(self.collidableMatrices[color], switch:getGridPos())
			if targetCell then
				--switch is on
				switch:activate()
				table.insert(onSwitches, switch) --doesn't rly matter what we put here as long as it's true-y
			else
				switch:deactivate()
			end
		end
		if #onSwitches == #switchArray then doorsToUnlock[color] = true else doorsToUnlock[color] = false end
	end

	return doorsToUnlock
end

function Room:isInBounds(cell)
	if cell.x > 0 and cell.x <= gridWidth and
	  cell.y > 0 and cell.y <= gridHeight then
		return true
	end
	return false
end

function Room:nextRoomCheck()
	for _, color in ipairs(self:getActiveColors()) do
		if not self:isInBounds(self.player[color]:getGridPos()) then
			nextRoom(self.player[color]:getGridPos())
			return
		end
	end
end


function Room:createEntity(class, color, x, y)
	if class == "Door" then
		self:registerDoor(Door:new(x, y))
	elseif class == "Wall" then
		self:registerCollidable(Wall:new(x, y, color), {color})
	elseif class == "Switch" then
		self.registerSwitch(Switch:new(x, y), color)
	elseif class == "player" then
		self:registerPlayer(Player:new(x, y, color), color)
	elseif class == "Box" then
		self:registerCollidable(Box:new(x, y, color), {color})
	elseif class == "npc" then
		-- do something soon
	elseif class == "Ass" then
		--do this later when we have a more definite schema
		-- self:registerAssorted(Ass:new(x, y, color, name))
	end
end

function Room:registerCollidable(entityPointer, colorArray) 
	if type(colorArray) ~= "table" then colorArray = {colorArray} end
	for _, color in ipairs(colorArray) do
		table.insert(self.collideables[color], entityPointer)
	end
end

function Room:registerDoor(entityPointer)
	table.insert(self.doors, entityPointer)
	self:registerCollidable(entityPointer, {"red", "green", "blue"})
end

function Room:registerSwitch(entityPointer, color)
	table.insert(self.switches[color], entityPointer)
end

function Room:registerPlayer(entityPointer, color)
	table.insert(self.colorsPlayerHas, color)
	self.player[color] = entityPointer
end

function Room:load(name)
	local room = love.filesystem.read("rooms/" .. name .. ".sav")
	self:deserialize(TSerial.unpack(room))
end

function Room:deserialize(saveTable)
	for entityClass, entities in pairs(saveTable) do
		for _, entity in pairs(entities) do
			self:createEntity(entityClass, entity.color, entity.x, entity.y)
		end
	end
end

function Room:save()
	local name = self.name or "test"
	local room = TSerial.pack(self:serialize(), nil, true)

	local success = love.filesystem.write("rooms/" .. name .. ".sav", room)
	if not success then error "great job u fool" end
end

function Room:serialize()
	local level = {}

	level.player = {}
	for _, color in ipairs(self:getInactiveColors()) do
		level.player[color] = self:getPlayerObject(color):getSerializationTable()
	end

	level.door = {}
	for _, door in pairs(self:getDoors()) do
		table.insert(level.door, doors:getSerializationTable())
	end


	for color, collideableArray in pairs(self:getCollideables()) do
		for _, collideable in pairs(collideableArray) do
			if not collideable:isInstanceOf("Door") then --for now, we might change doors up some
				local entityClass = collideable:getClass()
				if not level[entityClass] then level[entityClass] = {} end
				table.insert(level[entityClass], collideable:getSerializationTable())
			end
		end
	end

	level.switch = {}
	for color, switchArray in pairs(self:getSwitches()) do
		for _, switch in ipairs(switchArray) do
			table.insert(level.switch, switch:getSerializationTable())
		end
	end

	return level
end

function nextRoom(pos)
	local exit
	local newPos = {x = pos.x, y = pos.y}
	if pos.x > gridWidth then
		exit = "right"
		newPos.x = 1
	elseif pos.x < 1 then
		exit = "left"
		newPos.x = gridWidth
	elseif pos.y > gridHeight then
		exit = "bottom"
		newPos.y = 1
	elseif pos.y < 1 then
		exit = "top"
		newPos.y = gridHeight
	end

	currentRoom:save()

	currentRoom = Room:new{ 
		name = "test",
		player = {
			x = newPos.x, y = newPos.y, activeColors = currentRoom:getActiveColors()
		}
	}

end

