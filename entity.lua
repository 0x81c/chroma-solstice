Entity = class("Entity")

function Entity:initialize(t)
	self.x, self.y = t.x, t.y
	self.spriteSize = {w = t.sprite:getWidth(), h = t.sprite:getHeight()}
	self.size = {w = self.spriteSize.w / 16, h = self.spriteSize.h / 16}
	self.cellShape = t.cellShape or self:cellShapeFromBox(self.size)
	self.name = t.name
	self.color = Color:new(t.color)
	self.collision = t.collision --"none", "moveable", "unmoveable"
	self.sprite = t.sprite
	self.shaAmount = t.sha or 1
	self.onBump = t.onBump or function(self) return true end
	self.tick = t.tick or function(self) return true end

	self.drawOffset = {x = 0, y = 0}
	if self.size.w < 1 or self.size.h < 1 then
		self.drawOffset.x = (drawScale * 16 * self.size.w) / 2
		self.drawOffset.y = (drawScale * 16 * self.size.h) / 2
	end
end

function Entity:getCollision()
	return self.collision
end

function Entity:atGridPos(x, y)
	-- we probably don't need this anymore
	local size = self:getGridSize()
	if x >= self.x and y >= self.y and x < self.x + size.x and y < self.y + size.y then
	  return true
	end
	return false
end

function Entity:getOccupiedCells(coords)
	--we have this offset so we can project what a movement would do
	local offset = coords or {x = 0, y = 0}
	local cells = {}
	local pos = self:getGridPos()

	for _, cell in ipairs(self:getCellShape()) do
		local adjustedCell = {}

		adjustedCell.x = pos.x + cell.x - 1 + offset.x
		adjustedCell.y = pos.y + cell.y - 1 + offset.y
		table.insert(cells, adjustedCell)
	end
	
	return cells
end

function Entity:cellShapeFromBox(box)
	local cells = {}
	for i = 1, box.w do
		for j = 1, box.h do
			table.insert(cells, {x = i, y = j})
		end
	end
	return cells
end

function Entity:getCellShape()
	return self.cellShape
end

function Entity:getDrawBox()
	local drawPos = self:getDrawPos()
	return {x = drawPos.x, y = drawPos.y, w = self.spriteSize.w * drawScale, h = self.spriteSize.h * drawScale}
end

function Entity:getGridBox()
	return {x = self.x, y = self.y, w = self.size.w, h = self.size.h}
end

function Entity:getGridPos()
	return {x = self.x, y = self.y}
end

function Entity:getDrawPos()
	local gridPos = self:getGridPos()
	return {
		x = (gridPos.x - 1) * width / gridWidth + self:sha(),
		y = (gridPos.y - 1) * height / gridHeight + self:sha()
	}
end

function Entity:getGridSize()
	return {x = self.size.w, y = self.size.h}
end

function Entity:getname()
	return self.name
end

function Entity:tick()
	return self.tick()
end

function Entity:draw()
	love.graphics.setColor(self.color:set())
	local p = self:getDrawPos()
	love.graphics.draw(self.sprite, p.x + self.drawOffset.x, p.y + self.drawOffset.y, 0, drawScale, drawScale)
end

function Entity:canMove()
	if self:getCollision() == "moveable" then
		return true
	end
	return false
end

function Entity:canPassOver()
	if self:getCollision() == "none" then 
		return true
	end
	return false
end

function Entity:getClass()
	return self.class.name
end

function Entity:getSerializationTable()
	local pos = self:getGridPos()
	local table = {}
	table.x, table.y, table.color = pos.x, pos.y, self.color:getname()
	if self.name then table.name = self.name end
	return table
end

function Entity:move(axis, direction)
	self[axis] = self[axis] + direction
end

function Entity:setSprite(sprite)
	self.sprite = sprite
end

function Entity:sha()
	return math.random(-worldSha, worldSha) * .5
end

function Entity:bump()
	return self:onBump()
end


Entity.static.positionsAreEqual = function(cell1, cell2)
	if cell1.x == cell2.x and cell1.y == cell2.y then
		return true
	end
	return false
end
