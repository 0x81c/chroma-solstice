var gm = require('gm').subClass({ imageMagick: true });
var fs = require('fs')
var path = require('path')

var imageTypes = [".png", ".jpg", ".jpeg", ".bmp"]

if (typeof process.argv[3] === 'undefined')
  outDirectory = "processed/";
else
  outDirectory = process.argv[3];

var inDirectory = process.argv[2];
var outDirectory;
var palette = "palette"
var channels = ["Red", "Green", "Blue"]

var objToSerialize = {
  //format:

  //name_of_picture: filename
}


var ratio = function(x, y) {
  var div = x / y;

  var newRatio;
  if (1.333 < div < 1.666) {
    newRatio = 1.5
  }
  else {
    newRatio = Math.max(Math.min(Math.round(div), 3), 1)
  }

  var newX = y * newRatio;

  if ( newX < x) {
    x = newX;
  }
  else {
    y = x / newRatio;
  }

  return [x, y, newRatio]
}

fs.readdir(inDirectory, function(err, files) {
  files.forEach(function(file) {
    if ( imageTypes.indexOf(path.extname(file)) > -1 ) {
      
      //lets put the filename in an object
      var filename = file.split(".")[0]

      function processImage(filename, callbac, color) {
        var img = gm(filename);

        var w; var h; var w_off; var h_off;
        var ar_w; var ar_h;
        var r;
        var scale_w; var scale_h;


        img.size(function(err, value) {
          if (value) {
            w = value.width; h = value.height;
            if (w > h) {
              var res = ratio(w, h)
              ar_w = res[0];
              ar_h = res[1];
              r = res[2];

              scale_w = 48 * r;
              scale_h = 48;
            }
            else {
              var res = ratio(h, w);
              ar_w = res[1];
              ar_h = res[0];
              r = res[2];
              scale_w = 48;
              scale_h = 48 * r;
            }
            if (w > ar_w) {
              w_off = (w - ar_w) / 2;
              h_off = 0;
            }
            else if (h > ar_h) {
              w_off = 0;
              h_off = (h - ar_h) / 2
            }
            else {
              w_off = 0; h_off = 0;
            }
            console.log(r);
          }
          callbac({
            img: img, color: color, w: w, h: h, w_off: w_off, h_off: h_off, ar_w: ar_w,
            ar_h: ar_h, scale_w: scale_w, scale_h: scale_h
          });
        })
      }

      function cropScaleMap(obj) {
        obj.img.crop(obj.ar_w, obj.ar_h, obj.w_off, obj.h_off)
          .scale(obj.scale_w, obj.scale_h)
          // .map(palette)
          // .dither(true)
          // .channel(obj.color)
          .write(outDirectory + filename + "_" + obj.color.charAt(0).toLowerCase() + ".png", function(err) {

            // console.log(err);
            // console.log(outDirectory + filename + "_" + obj.color.charAt(0).toLowerCase() + ".png")
            // var col = obj.color.toLowerCase()
            // if (objToSerialize[filename]) {
            //   objToSerialize[filename][col] = outDirectory + filename + "_" + obj.color.charAt(0).toLowerCase() + ".png"
            // }
            // else {
            //   objToSerialize[filename] = {}
            //   objToSerialize[filename][col] = outDirectory + filename + "_" + obj.color.charAt(0).toLowerCase() + ".png"
            // }
            // fs.writeFile('art.json', JSON.stringify(objToSerialize), function(err) {
            //   if (err) throw err;
            //   console.log("wowwwww");
            // });
          });

      }

      //do the stuff
      for (var i = 0; i < channels.length; i++) {
        // var c = channels[i];
        // var c_abb = channels[i].charAt(0).toLowerCase();
        
        processImage(file, cropScaleMap, channels[i])
      }
    }
  });
  var s = JSON.stringify(objToSerialize);
  console.log(s)
});

