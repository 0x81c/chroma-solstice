mkdir -p processed

for f in *.png
do
	NAME=`echo "$f" | cut -d'.' -f1`

	echo "mapping $f to palette..."

	convert $f +dither -map ../palette "processed/${NAME}_reduced.png"

	TARGET="processed/${NAME}_reduced.png"

	echo "extracting colors from $f..."

	convert $TARGET -channel R -separate "processed/${NAME}_r.png"
	convert $TARGET -channel G -separate "processed/${NAME}_g.png"
	convert $TARGET -channel B -separate "processed/${NAME}_b.png"

	echo "done!!!"
done
