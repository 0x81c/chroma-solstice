#!/bin/bash

echo "extracting colors from $1..."

NAME=`echo "$1" | cut -d'.' -f1`

convert $1 -channel R -separate "${NAME}_r.png"
convert $1 -channel G -separate "${NAME}_g.png"
convert $1 -channel B -separate "${NAME}_b.png"

echo "done!!!"