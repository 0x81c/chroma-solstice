-- this module provides some helpful functions
-- for setting color!!

Color = class("Color")

Color.static.list = {
	red = {r = 255, g = 0, b = 0, a = 255},
	green = {r = 0, g = 255, b = 0, a = 255},
	blue = {r = 0, g = 0, b = 255, a = 255},
	white = {r = 255, g = 255, b = 255, a = 255}
}

function Color:initialize(inColor)
	self.name = inColor
	local c = Color.list[inColor]
	self.r, self.g, self.b, self.a = c.r, c.g, c.b, c.a
end

function Color:set()
	return self.r, self.g, self.b, self.a
end

function Color:getname()
	return self.name
end

function randomColor()
	local colors = {"red", "green", "blue"}
	return colors[math.random(1, 3)]
end