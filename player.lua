require "entity"

Player = class("Player", Entity)

Player.static.sounds = {
	switchFail = love.audio.newSource("sounds/switch_fail.wav"),
	switchSuccess = love.audio.newSource("sounds/switch_success.wav")
}

function Player:initialize(x, y, color)
	Entity.initialize(self, {
		x = x,
		y = y,
		color = color,
		collision = "none", --player can't push other player
		sprite = sprites.player
	})
end

function Player:setActiveColors(colorArray)
	self.activeColors = colorArray
end

function Player:getActiveColors()
	return self.activeColors
end

function Player:update()

end

function Player:playSwitchSound(success)
	local sound
	if success then
		sound = Player.sounds.switchSuccess
	else
		sound = Player.sounds.switchFail
	end
	sound:stop()
	sound:play()
end
