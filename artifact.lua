require "entity"

Artifact = class("Artifact", Entity)

function Artifact:initialize(x, y, color)
	Entity.initialize(self, {
		x = x,
		y = y,
		color = color,
		collision = "unmoveable",
		sprite = sprites.artifact
	})
end