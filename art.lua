require "entity"

Art = class("Art", Entity)

function Art:initialize(x, y, color, name)
	local spr = love.graphics.newImage("art/museum/to_process/cropped/processed/" .. name .. "_" .. string.lower(string.sub(color, 1, 1)) ..".png")
	Entity.initialize(self, {
		x = x,
		y = y,
		color = color,
		collision = "moveable", --player can't push other player
		sprite = spr
	})
end


function Art:draw()
	love.graphics.setColor(self.color:set())
	local p = self:getDrawBox()
	love.graphics.setLineWidth(drawScale) --equivalent of one pixel
	love.graphics.rectangle("line", p.x, p.y, p.w, p.h)
	love.graphics.draw(self.sprite, p.x, p.y, 0, drawScale, drawScale)
end