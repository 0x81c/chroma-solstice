wiggle = love.graphics.newShader[[

  extern number seed;
  extern number amount;


  int grain = 10; //higher = less, lower = more

  float rand(vec2 co){
      return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
  }

	vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords) {
   		vec4 pixel_me = Texel(texture, texture_coords );

      return Texel(texture, texture_coords + (rand(texture_coords * seed) - .5) * amount);
      /*  		
        int resolution = int(12 * 1 / zoom);

        vec2 final_coords = floor((resolution * texture_coords) + 0.5) / resolution;

        vec4 averaged_pixel = (Texel(texture, final_coords - (1 / resolution)) + Texel(texture, final_coords)) / 2 + (rand(final_coords * seed) / grain);

        averaged_pixel.a = 1;

        pixel_me = Texel(texture, final_coords);

   		  return averaged_pixel;
      */
	}
]]

glow = love.graphics.newShader[[
      vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords) {
        vec4 pixel_me = Texel(texture, texture_coords );
        if (pixel_me.rgb != vec3(0,0,0))
          return pixel_me;
        
        int samples = 5;
        float intensity = 60;
        float radius = .1;

        for (int i = 1; i < samples; i++ ) {
          vec4 pixel_left = Texel(texture, texture_coords + vec2(-radius * i, 0)); 
          vec4 pixel_right = Texel(texture, texture_coords + vec2(radius * i, 0)); 
          vec4 pixel_top = Texel(texture, texture_coords + vec2(0, radius * i)); 
          vec4 pixel_bottom = Texel(texture, texture_coords + vec2(0, -radius * i));
          vec4 pixel_top_left = Texel(texture, texture_coords + vec2(-radius * i, radius * i)); 
          vec4 pixel_top_right = Texel(texture, texture_coords + vec2(radius * i, radius * i)); 
          vec4 pixel_bottom_left = Texel(texture, texture_coords + vec2(-radius * i, -radius * i)); 
          vec4 pixel_bottom_right = Texel(texture, texture_coords + vec2(radius * i, -radius * i));

          pixel_me += (pixel_left + pixel_right + pixel_top + pixel_bottom + pixel_bottom_right + pixel_bottom_left + pixel_top_right + pixel_top_left) / (intensity * i);

          intensity -= 2.5;

        }
        return pixel_me;
      }
    ]]